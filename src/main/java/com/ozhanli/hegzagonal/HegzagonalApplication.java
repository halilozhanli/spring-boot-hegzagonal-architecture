package com.ozhanli.hegzagonal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HegzagonalApplication {

	public static void main(String[] args) {
		SpringApplication.run(HegzagonalApplication.class, args);
	}

}
