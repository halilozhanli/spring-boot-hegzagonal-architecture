package com.ozhanli.hegzagonal.domain;

public enum OrderStatus {
    CREATED, COMPLETED
}