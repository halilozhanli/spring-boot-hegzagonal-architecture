package com.ozhanli.hegzagonal.domain.repository;

import com.ozhanli.hegzagonal.domain.Order;

import java.util.Optional;
import java.util.UUID;

public interface OrderRepository {

    Optional<Order> findById(UUID id);

    void save(Order order);

}