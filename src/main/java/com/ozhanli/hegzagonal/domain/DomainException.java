package com.ozhanli.hegzagonal.domain;

class DomainException extends RuntimeException {
    DomainException(final String message) {
        super(message);
    }
}