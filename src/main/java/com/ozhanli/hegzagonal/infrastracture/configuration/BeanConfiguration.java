package com.ozhanli.hegzagonal.infrastracture.configuration;

import com.ozhanli.hegzagonal.HegzagonalApplication;
import com.ozhanli.hegzagonal.domain.repository.OrderRepository;
import com.ozhanli.hegzagonal.domain.service.DomainOrderService;
import com.ozhanli.hegzagonal.domain.service.OrderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = HegzagonalApplication.class)
public class BeanConfiguration {

    @Bean
    OrderService orderService(final OrderRepository orderRepository) {
        return new DomainOrderService(orderRepository);
    }

}